# Detector-Short-URL
Detector-Short-URL

Esta herramienta sirve para detectar la procedencia de un enlace acortado.

Created by: Otaku-dev ^^


Pasos de Instalación:

Actualizamos Repositorios


apt update && apt upgrade -y


Instalamos Dependencias...


apt install git -y


Clonamos El Repositorio:


git clone https://gitlab.com/Otaku-dev/Detector-Short-URL


Seleccionamos el Fichero:


cd Detector-Short-URL


Listamos :

ls


Damos permisos de Ejecución:


chmod u+x  detect-shorturl.sh


Ejecutamos :


bash  detect-shorturl.sh

#Para instalar el paquete .deb

dpkg -i detectShortUrl_0.1_all.deb

#Se ejecuta a nivel global

detect-shorturl.sh

Listo!!


Created by: Otaku-dev ^^


